package Questions;

public class fast_power {
    public static int pow(int x,int n){
        if(n==1)
            return x;
        int res=pow(x,n/2);
        if(n%2==0)
            return res*res;
        else
            return res*res*x;
    }
    public static void main(String[] args) {
        System.out.println(pow(2,10));
    }
}
