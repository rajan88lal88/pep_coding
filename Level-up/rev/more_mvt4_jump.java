/**
 * 8d_mvt
 */
public class more_mvt4_jump {
    static int[][] dir={
        {0,1},
        {1,0},
        {0,-1},
        {-1,0}
    };
    static String [] name={
        "R","D","L","U"
    };
    public static int paths(boolean [][]vis,int si,int sj,int ei,int ej,String path){
        
        if(si>ei||sj>ej||si<0||sj<0||vis[si][sj]==true)
            return 0;
        if(si==ei&&sj==ej)
        {
            System.out.println(path);
            return 1;
        }    
        int count=0;
        vis[si][sj]=true;
        for(int i=0;i<dir.length;i++){
            for(int j=1;j<ei;j++)
            count+=paths(vis,si+j*dir[i][0],sj+j*dir[i][1],ei,ej,path+name[i]+j);
        }
        vis[si][sj]=false;
        return count;
    }

    public static void main(String[] args) {
        System.out.println(paths(new boolean[4][5],0,0,3,3,""));
    }
}