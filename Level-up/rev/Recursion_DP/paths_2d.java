package Recursion_DP;

public class paths_2d {
    public static int total_path_HV_rec(int n,int m,int i, int j){
        if(i>=n||j>=m)
            return 0;
        if(i==n-1&&j==m-1)
            return 1;
        int max=n>m?n:m;
        int res=0;
        for(int t=1;t<max;t++){
            res+=total_path_HV_rec(n, m, i+t, j);
            res+=total_path_HV_rec(n, m, i, j+t);
        }
        return res;
    }
    public static int total_path_HV_mem(int n,int m,int i, int j,int [][]dp){
        if(i>=n||j>=m)
            return 0;
        if(i==n-1&&j==m-1)
            return 1;
        int max=n>m?n:m;
        int res=0;
        if(dp[i][j]!=0)
            return dp[i][j];
        for(int t=1;t<max;t++){
            res+=total_path_HV_mem(n, m, i+t, j,dp);
            res+=total_path_HV_mem(n, m, i, j+t,dp);
        }
        return dp[i][j]=res;
    }

    public static int total_path_HV_tab(int n,int m){
        int dp[][]=new int [n][m];
        dp[n-1][m-1]=1;
        int max=n>m?n:m;
        for(int i=n-1;i>=0;i--){
            for(int j=m-1;j>=0;j--){
                if(i==n-1&&j==m-1)
                    continue;
                int res=0;
                for(int t=1;t<max;t++){
                    if(i+t<n)
                        res+=dp[i+t][j];
                    if(j+t<m)
                        res+=dp[i][j+t];
                }
                dp[i][j]=res;
            }
        }
        return dp[0][0];
    }


    public static int total_path_HVD_rec(int n,int m,int i, int j){
        if(i>=n||j>=m)
            return 0;
        if(i==n-1&&j==m-1)
            return 1;
        int max=n>m?n:m;
        int res=0;
        for(int t=1;t<max;t++){
            res+=total_path_HVD_rec(n, m, i+t, j);
            res+=total_path_HVD_rec(n, m, i, j+t);
            res+=total_path_HVD_rec(n, m, i+t, j+t);
        }
        return res;
    }
    public static int total_path_HVD_mem(int n,int m,int i, int j,int [][]dp){
        if(i>=n||j>=m)
            return 0;
        if(i==n-1&&j==m-1)
            return 1;
        int max=n>m?n:m;
        int res=0;
        if(dp[i][j]!=0)
            return dp[i][j];
        for(int t=1;t<max;t++){
            res+=total_path_HVD_mem(n, m, i+t, j,dp);
            res+=total_path_HVD_mem(n, m, i, j+t,dp);
            res+=total_path_HVD_mem(n, m, i+t, j+t,dp);
        }
        return dp[i][j]=res;
    }

    public static int total_path_HVD_tab(int n,int m){
        int dp[][]=new int [n][m];
        dp[n-1][m-1]=1;
        int max=n>m?n:m;
        for(int i=n-1;i>=0;i--){
            for(int j=m-1;j>=0;j--){
                if(i==n-1&&j==m-1)
                    continue;
                int res=0;
                for(int t=1;t<max;t++){
                    if(i+t<n)
                        res+=dp[i+t][j];
                    if(j+t<m)
                        res+=dp[i][j+t];
                    if(i+t<n&&j+t<m)
                        res+=dp[i+t][j+t];
                }
                dp[i][j]=res;
            }
        }
        return dp[0][0];
    }

    public static void main(String[] args) {
        // System.out.println(total_path_HV_rec(4, 4, 0, 0));
        // System.out.println(total_path_HV_mem(4, 4, 0, 0,new int[4][4]));
        // System.out.println(total_path_HV_tab(3, 3));
        // System.out.println(total_path_HVD_rec(4, 4, 0, 0));
        // System.out.println(total_path_HVD_mem(4, 4, 0, 0,new int[4][4]));
        System.out.println(total_path_HVD_tab(4, 4));

    }
    
}
