package Recursion_DP;

import java.util.Scanner;
/**
 * gfg_partition_into_k_subsets
 */
public class gfg_partition_into_k_subsets {
    public static int subsets(int n,int k,int dp[][]){
        if(k==0||n==0||k>n)
            return dp[n][k]=0;
        if(k==1)
            return dp[n][k]=1;
        if(dp[n][k]!=0)
            return dp[n][k];
        return dp[n][k]=subsets(n-1,k,dp)*k+subsets(n-1, k-1,dp);
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n=s.nextInt();
        int k=s.nextInt();
        System.out.println(subsets(n,k,new int[n+1][k+1]));
    }
}