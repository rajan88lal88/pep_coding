package Recursion_DP;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class dice_path {

    public static int board_path(int i, int n, int dp[]) {
        if (i == n)
            return 1;
        if (i > n)
            return 0;
        if (dp[i] != 0)
            return dp[i];
        int res = 0;
        for (int t = 1; t <= 6; t++) {
            res += board_path(i + t, n, dp);
        }
        return dp[i] = res;
    }

    public static int board_path_tab(int idx, int n) {
        int dp[] = new int[11];
        dp[n] = 1;
        for (int i = n - 1; i >= idx; i--) {
            int res = 0;
            for (int t = 1; t <= 6; t++) {
                if (i + t <= n)
                    res += dp[i + t];
            }
            dp[i] = res;
        }
        return dp[idx];
    }

    public static int board_path_opti(int n) {// O(1) space
        Deque<Integer> dq=new ArrayDeque<>();
        dq.addFirst(1);
        int i=0;
        while(i<n){
            // System.out.println(dq.peekFirst());
            if(dq.size()==1)
                dq.addFirst(1);
            else if(dq.size()<7)
                dq.addFirst(dq.peekFirst()*2);
            else
                dq.addFirst((dq.peekFirst()*2)-dq.removeLast());
            i++;
            
        }
        return dq.peekFirst();
    }
    public static int custom_dice_board_path(int i,int n,int [] dice,int []dp){
        if(i==n)
            return 1;
        if(i>n)
            return 0;
        if(dp[i]!=0)
            return dp[i];
        int res=0;
        for(int t=1;t<dice.length;t++){
            res+=custom_dice_board_path(i+dice[t], n, dice,dp);
        }
        return dp[i]=res;
    }
    public static void main(String[] args) {
        System.out.println(board_path(0, 10,new int[11]));
        // System.out.println(board_path_tab(0, 10));
        // System.out.println(board_path_opti(10));

        // int []dice={2,4,3,8};
        // System.out.println(custom_dice_board_path(0, 10,dice,new int[11]));
    }
}
