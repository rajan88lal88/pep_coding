package Recursion_DP;

import java.util.Scanner;

public class gfg_gold_mine {

    public static int maxmine(int i,int j,int [][]arr,int dp[][]){
        if(i>=arr.length||i<0)
            return -1;
        if(j==arr[0].length-1)
            return dp[i][j]=arr[i][j];
        int res=0;
        if(dp[i][j]!=0)
            return dp[i][j];
        res=Integer.max(res,maxmine(i,j+1,arr,dp));
        res=Integer.max(res,maxmine(i-1,j+1,arr,dp));
        res=Integer.max(res,maxmine(i+1,j+1,arr,dp));
        return dp[i][j]=res+arr[i][j];
    }
    public static int goldmine(int arr[][]){
        int res=0;
        int dp[][]=new int[arr.length][arr[0].length];
        for(int i=0;i<arr.length;i++){
            res=Integer.max(res,maxmine(i,0,arr,dp));
        }
        return res;
    }
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        int t=s.nextInt();
        for(int i=0;i<t;i++)
        {
            int n=s.nextInt();
            int m=s.nextInt();
            int arr[][]=new int[n][m];
            for(int j=0;j<n;j++)
                for(int k=0;k<m;k++)
                    arr[j][k]=s.nextInt();
            int res=goldmine(arr);
            System.out.println(res);
        }
    }
    
}
