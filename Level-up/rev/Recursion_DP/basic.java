package Recursion_DP;

public class basic {

    public static int fib_rec(int n){
        if(n<=1)
            return n;
        return fib_rec(n-1)+fib_rec(n-2);
    }
    public static int fib_mem(int n,int []dp){
        if(n<=1)
            return dp[n]=n;
        int a=dp[n-2]!=0?dp[n-2]:fib_mem(n-2, dp);
        int b=dp[n-1]!=0?dp[n-1]:fib_mem(n-1, dp);
        return dp[n]=a+b;
    }
    public static int fib_tab(int n){
        int dp[]=new int[n+1];
        dp[0]=0;
        dp[1]=1;
        for(int i=2;i<=n;i++){
            dp[i]=dp[i-1]+dp[i-2];
        }
        return dp[n];
    }
    public static int fib_opti(int n){
        if(n<=1)
            return n;
        int a=0,b=1;
        for(int i=2;i<=n;i++){
            int c=a+b;
            a=b;b=c;
        }
        return b;
    }
    public static void main(String[] args) {
        // System.out.println(fib_rec(6));
        // System.out.println(fib_mem(6, new int[7]));
        System.out.println(fib_opti(6));

    }
    
}
