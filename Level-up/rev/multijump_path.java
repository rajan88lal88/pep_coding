/**
 * multijump_path
 */
public class multijump_path {

    public static int path(int si,int sj,int ei,int ej,String path){
        if(si>ei||sj>ej)
            return 0;
        if(si==ei&&sj==ej){
            System.out.println(path);
            return 1;
        }
        int count =0;
        for(int i=1;i<=ej-sj;i++)
            count+=path(si,sj+i,ei,ej,path+"H"+i);
        for(int i=1;i<=ei-si;i++)
            count+=path(si+i,sj,ei,ej,path+"V"+i);
        for(int i=1;i<=ej-sj&&i<=ei-si;i++)
            count+=path(si+1,sj+i,ei,ej,path+"D"+i);
        return count;
    }

    public static void main(String[] args) {
        System.out.println(path(0,0,2,2,""));
    }
}