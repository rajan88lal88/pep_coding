package Stack;

import java.util.Arrays;
import java.util.Stack;

public class next_small_greater {

    public static void display(int arr[]){
        for(int i=0;i<arr.length;i++)
            System.out.print(arr[i]+"\t");
        System.out.println();
    }

    public static int[] prev_greater(int arr[]){
        int res[]=new int [arr.length];
        Arrays.fill(res, -1);
        Stack<Integer> st=new Stack<>();
        for(int i=arr.length-1;i>=0;i--){
            while(st.size()>0&&arr[st.peek()]<arr[i])
                res[st.pop()]=i;
            st.push(i);

        }
        // display(res);
        for(int i=0;i<res.length;i++)
        {
            if(res[i]>=0&&res[i]<res.length)
                res[i]=arr[res[i]];
        }
        return res;
    }

    public static int[] prev_smaller(int arr[]){
        int res[]=new int [arr.length];
        Arrays.fill(res, -1);
        Stack<Integer> st=new Stack<>();
        for(int i=arr.length-1;i>=0;i--){
            while(st.size()>0&&arr[st.peek()]>arr[i])
                res[st.pop()]=i;
            st.push(i);

        }
        // display(res);
        for(int i=0;i<res.length;i++)
        {
            if(res[i]>=0&&res[i]<res.length)
                res[i]=arr[res[i]];
        }
        return res;
    }
    
    public static int[] next_smaller(int arr[]){
        int res[]=new int [arr.length];
        Arrays.fill(res, arr.length);
        Stack<Integer> st=new Stack<>();
        for(int i=0;i<arr.length;i++){
            while(st.size()>0&&arr[st.peek()]>arr[i])
                res[st.pop()]=i;
            st.push(i);

        }
        // display(res);
        for(int i=0;i<res.length;i++)
        {
            if(res[i]>=0&&res[i]<res.length)
                res[i]=arr[res[i]];
        }
        return res;

    }

    public static int[] next_greater(int arr[]){
        int res[]=new int [arr.length];
        Arrays.fill(res, arr.length);
        Stack<Integer> st=new Stack<>();
        for(int i=0;i<arr.length;i++){
            
            while(st.size()>0&&arr[st.peek()]<arr[i])
                res[st.pop()]=i;
            st.push(i);

        }
        
        for(int i=0;i<res.length;i++)
        {
            if(res[i]>=0&&res[i]<res.length)
                res[i]=arr[res[i]];
        }
        return res;
    }

    public static void main(String[] args) {
        int arr[]={7,4,2,1,3,5,38,7,4,2,3,20,12,6};
        display(arr);
        int res[]=next_greater(arr);
        display(res);
        // res=next_smaller(arr);
        // display(res);
        // res=prev_greater(arr);
        // display(res);
        // res=prev_smaller(arr);
        // display(res);
    }
    
}