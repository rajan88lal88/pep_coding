package Stack;

public class stack<T> {
    private T[] st;
    private int size,maxSize,tos;
    public stack(){
        setValues(10);
    }
    public stack(int n){
        setValues(n);
    }
    @SuppressWarnings("unchecked")
    protected void setValues(int n){
        this.st= (T[]) new Object[n];
        this.size=0;
        this.maxSize=n;
        this.tos=-1;
    }

    public boolean isEmpty(){
        return this.size==0;
    }

    public int size(){
        return this.size;
    }

    public int capacity(){
        return this.maxSize;
    }
    protected void _push(T data){
        this.st[++this.tos] = data;
        size++;
    }
    public void push(T data) throws Exception {
        if (size==maxSize) {
            throw new Exception("Stack Full");
        }
        _push(data);
        
    }

    private T _top(){
        return this.st[this.tos];
    }

    public T top() throws Exception{
        if (this.tos == -1) {
            throw new Exception("Stack Empty");
        }
        return _top();
    }

    protected T _pop() {
        T rv = this.st[this.tos];
        this.tos--;
        size--;
        return rv;
    }

    public T pop() throws Exception{
        if (this.size==0) {
            throw new Exception("Stack Empty");
        }
        return _pop();
    }

    @Override
    public String toString(){
        StringBuilder sb=new StringBuilder();
        sb.append("[ ");
        for(int i=this.tos;i>=0;i--){
            sb.append(st[i]);
            if(i!=0) sb.append(", ");
        }
        sb.append(" ]");
        return sb.toString();
    }
}