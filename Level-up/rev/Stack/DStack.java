package Stack;
public class DStack<T> extends stack<T>{
    public DStack(){
        super();
    }
    public DStack(int n){
        super(n);
    }
    private void push_arr(T arr[]){
        for(T x:arr){
            super._push(x);
        }
    }
    public DStack(T arr[]){
        super(2*arr.length);
        push_arr(arr);
    }
    

    @Override
    @SuppressWarnings("unchecked")
    public void push(T data){
        if(this.size()==this.capacity()){
            T tmp[]=(T[])new Object[this.capacity()];
            for(int i=this.capacity()-1;i>=0;i--)
                tmp[i]= (T) this._pop();
            this.setValues(2*this.capacity());
            this.push_arr(tmp);
        }
        super._push(data);
    }
}