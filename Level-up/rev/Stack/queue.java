package Stack;

public class queue<T> {
    private T[] st;
    private int size,maxSize;
    protected int head,tail;

    public queue(){
        setValues(10);
    }
    public queue(int n){
        setValues(n);
    }
    @SuppressWarnings("unchecked")
    protected void setValues(int n){
        this.st= (T[]) new Object[n];
        this.size=0;
        this.maxSize=n;
        this.head=-1;
        this.tail=-1;
    }

    public boolean isEmpty(){
        return this.size==0;
    }

    public int size(){
        return this.size;
    }

    public int capacity(){
        return this.maxSize;
    }
    protected void _push(T data){
        if(tail==-1)
        {
            head=tail=0;
        }
        else
        {
            tail=(tail+1)%this.maxSize;
        }
        this.st[this.tail] = data;
        size++;
    }
    public void push(T data) throws Exception {
        if(this.size==this.maxSize){
            throw new Exception("Queue Full");
        }
        _push(data);
        
    }

    private T _top(){
        return this.st[this.head];
    }

    public T top() throws Exception{
        if (this.head == -1) {
            throw new Exception("Stack Empty");
        }
        return _top();
    }

    protected T _pop() {
        T rv = this.st[this.head];
        this.head=(this.head+1)%this.maxSize;
        size--;
        if(size==0)
        {
            head=tail=-1;
        }
        return rv;
    }

    public T pop() throws Exception{
        if (this.size==0) {
            throw new Exception("Stack Empty");
        }
        return _pop();
    }

    @Override
    public String toString(){
        StringBuilder sb=new StringBuilder();
        sb.append("[ ");
        for(int i=this.head;i!=tail;i++){
            if(i==this.maxSize)
                i=0;
            sb.append(st[i]+", ");
        }
        if(this.tail!=-1)
            sb.append(this.st[this.tail]);
        sb.append(" ]");
        return sb.toString();
    }
}