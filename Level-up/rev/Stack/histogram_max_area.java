package Stack;

import java.util.Stack;

public class histogram_max_area {
    public static int largestRectangleArea(int[] heights) {
        Stack <Integer> st=new Stack<>();
        int area=0;
        st.push(-1);
        for(int i=0;i<heights.length;i++){
            if(st.peek()==-1||heights[i]>heights[st.peek()])
                st.push(i);
            else{
                while(st.peek()!=-1&&heights[st.peek()]>heights[i]){
                    int t=st.pop();
                    int ar=(i-st.peek()-1)*heights[t];
                    area=Integer.max(area,ar);
                }
                st.push(i);
            }
            System.out.println(st.peek());
        }
        return area;
    }
    public static void main(String[] args) {
        int arr[]={2,1,5,6,2,3};
        System.out.println(largestRectangleArea(arr));
    }
    
}
