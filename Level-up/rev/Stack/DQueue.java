package Stack;

public class DQueue<T> extends queue<T> {
    public DQueue(){
        super();
    }
    public DQueue(int n){
        super(n);
    }
    private void push_arr(T arr[]){
        for(T x:arr){
            super._push(x);
        }
    }
    public DQueue(T arr[]){
        super(2*arr.length);
        push_arr(arr);
    }
    

    @Override
    @SuppressWarnings("unchecked")
    public void push(T data){
        if(this.size()==this.capacity()){
            T tmp[]=(T[])new Object[this.size()];
            int i=0;
            while(this.size()!=0) tmp[i++]=this._pop();
            this.setValues(2*this.capacity());
            this.push_arr(tmp);
        }
        super._push(data);
    }
}