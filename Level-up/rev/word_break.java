import java.util.*;

public class word_break {

    public static int wordBreak(String str,int idx,String ans,int maxlen,HashSet<String> words){
        if(idx==str.length()){
            System.out.println(ans);
            return 1;
        }
        int count=0;
        for(int i=idx+1;i<=idx+maxlen+1&&i<=str.length();i++){
            String word=str.substring(idx, i);
            if(words.contains(word)){
                count+=wordBreak(str, i, ans + word+" ", maxlen, words);
            }
        }
        return count;
    }
    public static void main(String[] args) {
        String words[]={"mobile","samsung","sam","sung","man","mango","icecream","and","go","i","like","ice","cream"}; 
        HashSet<String> hs=new HashSet<>();
        int maxlen=0;
        for(int i=0;i<words.length;i++)
        {
            if(words[i].length()>maxlen)
                maxlen=words[i].length();
            hs.add(words[i]);
        }
        String str="ilikesamsungandmango";
        System.out.println(wordBreak(str, 0, "", maxlen, hs));
    }
    
}