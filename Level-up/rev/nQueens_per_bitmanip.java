public class nQueens_per_bitmanip {

    static int rowQ=0;
    static int colQ=0;
    static int diaQ=0;
    static int adiaQ=0;

    public static int nQueens(int m,int row,int tnq,String ans){
        if(tnq==0){
            
            System.out.println(ans);
            return 1;
        }
        int count=0;
        // System.out.println("check");
        for(int i=0;i<m;i++){
            int r=row;
            int c=i;
        
            if((rowQ&(1<<r))==0&&(colQ&(1<<c))==0&&(diaQ&(1<<(r+c)))==0&&(adiaQ&(1<<(r-c+m)))==0){
                // System.out.println("check");
                rowQ^=(1<<r);
                colQ^=(1<<c);
                diaQ^=(1<<(r+c));
                adiaQ^=(1<<(r-c+m));
                
                count+=nQueens(m,row+1, tnq-1, ans+"("+r+" : "+c+")");
                
                rowQ^=(1<<r);
                colQ^=(1<<c);
                diaQ^=(1<<(r+c));
                adiaQ^=(1<<(r-c+m));
            }
        }
        return count;
    }
    public static void main(String[] args) {
        // System.out.println("check");
        System.out.println(nQueens(4, 0, 4, ""));
    }
    
}