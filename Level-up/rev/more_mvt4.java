/**
 * 8d_mvt
 */
public class more_mvt4 {
    static int[][] dir={
        {0,1},
        {1,0},
        {0,-1},
        {-1,0}
    };
    static String [] name={
        "R","D","L","U"
    };
    public static int paths(boolean [][]vis,int si,int sj,int ei,int ej,String path){
        
        if(si>ei||sj>ej||si<0||sj<0||vis[si][sj]==true)
            return 0;
        if(si==ei&&sj==ej)
        {
            System.out.println(path);
            return 1;
        }    
        int count=0;
        vis[si][sj]=true;
        for(int i=0;i<dir.length;i++){
            count+=paths(vis,si+dir[i][0],sj+dir[i][1],ei,ej,path+name[i]);
        }
        vis[si][sj]=false;
        return count;
    }

    public static void main(String[] args) {
        System.out.println(paths(new boolean[3][3],0,0,2,2,""));
    }
}