package codevita;

import java.util.*;
public class string_pair {
    
    public static int vowel_count(int n){
        String num=toword(n);
        int res=0;
        for(int i=0;i<num.length();i++)
        {
            if(num.charAt(i)=='a'||num.charAt(i)=='e'||num.charAt(i)=='i'||num.charAt(i)=='o'||num.charAt(i)=='u')
                res++;
        }
        return res;
    }

    static int getPairsCount(int arr[], int sum) 
    { 
        // int n=arr.length;
        // HashMap<Integer, Integer> hm = new HashMap<>(); 
        // for (int i=0; i<n; i++){ 
        //     if(!hm.containsKey(arr[i])) 
        //         hm.put(arr[i],0); 
        //     hm.put(arr[i], hm.get(arr[i])+1); 
        // } 
        // int twice_count = 0; 
        // for (int i=0; i<n; i++) 
        // { 
        //     if(hm.get(sum-arr[i]) != null) 
        //         twice_count += hm.get(sum-arr[i]);
        //     if (sum-arr[i] == arr[i]) 
        //         twice_count--; 
        // }
        // return twice_count/2; 
        int n=arr.length;
        int count=0;
        for(int i=0;i<n;i++){
            for(int j=i+1;j<n;j++){
                if(arr[i]+arr[j]==sum)
                    count++;
            }
        }
        return count;
    } 
    private static String toword(int n) {
        if(n==0)
            return "zero";
        if(n==100)
            return "hundred";
        if(n>100)
            return "greater 100";
        String[] tens = {
            "",
            "ten",
            "twenty ",
            "thirty ",
            "forty ",
            "fifty ",
            "sixty ",
            "seventy ",
            "eighty ",
            "ninety "
          };
        String[] ones = {
            "",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen"
          };
        String res;
    
        if (n % 100 < 20){
          res = ones[n % 100];
          n /= 100;
        }
        else {
          res = ones[n % 10];
          n /= 10;
    
          res = tens[n % 10] + res;
          n /= 10;
        }
        return res;
    }
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        int n=0;
        if(s.hasNext())
            n=s.nextInt();
        int arr[]=new int[n];
        int res=0;
        for(int i=0;i<n;i++)
            if(s.hasNext())
            {
                arr[i]=s.nextInt();
            }
        for(int i:arr){
            res+=vowel_count(i);
        }
        int pairs=getPairsCount(arr,res);
        System.out.println(toword(pairs));
        // for(int i=0;i<=100;i++)
        //     System.out.println(toword(i)+"\t\t\t"+vowel_count(i));
        s.close();
    }
    
}