package codevita;

import java.util.*;

public class cube {

    public static float length(float a,float b, float height){
        float base=Math.abs(a-b);
        return (float)Math.sqrt(height*height+base*base);
    }

    public static float area(float a,float b,float c){
        float s=(a+b+c)/2;
        return (float)Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }
    public static void main(String[] args) {
        int height=0;
        int sa=0,sb=0,sc=0,sd=0;
        int va=0,vb=0,vc=0,vd=0;
        int da=0,db=0,dc=0,dd=0;
        Scanner s=new Scanner(System.in);
        if(s.hasNext()){
            height=s.nextInt();
        }
        if(s.hasNext()){
            sa=s.nextInt();
            sb=s.nextInt();
            sc=s.nextInt();
            sd=s.nextInt();
        }
        if(s.hasNext()){
            va=s.nextInt();
            vb=s.nextInt();
            vc=s.nextInt();
            vd=s.nextInt();
        }
        if(s.hasNext()){
            String ch=s.next();
            if(ch.equals("U"))
                da=1;
            else
                da=-1;
            ch=s.next();
            if(ch.equals("U"))
                db=1;
            else
                db=-1;
            ch=s.next();
            if(ch.equals("U"))
                dc=1;
            else
                dc=-1;
            ch=s.next();
            if(ch.equals("U"))
                dd=1;
            else
                dd=-1;
        }
        float a,b,c,d,e;
        float abc,adc;
        float min=Float.MAX_VALUE;
        float max=0;
        while(true){

            // System.out.println(sa+":"+sb+":"+sc+":"+sd);

            a=length(sa,sb,height);
            b=length(sb,sc,height);
            c=length(sc,sd,height);
            d=length(sd,sa,height);
            e=length(sa,sc,height*(float)Math.sqrt(2));

            abc=area(a,b,e);
            adc=area(c,d,e);

            max=Float.max((float)Math.pow((abc+adc),2),max);
            min=Float.min((float)Math.pow((abc+adc),2),min);
            sa+=(va*da);
            sb+=(vb*db);
            sc+=(vc*dc);
            sd+=(vd*dd);

            if(sa<=0)
                sa=0;
            if(sa>=height)
                sa=height;
            if(sb<=0)
                sb=0;
            if(sb>=height)
                sb=height;
            if(sc<=0)
                sc=0;
            if(sc>=height)
                sc=height;
            if(sd<=0)
                sd=0;
            if(sd>=height)
                sd=height;
            if((sa==0||sa==height)&&(sb==0||sb==height)&&(sc==0||sc==height)&&(sd==0||sd==height))
                break;
        }
        a=length(sa,sb,height);
        b=length(sb,sc,height);
        c=length(sc,sd,height);
        d=length(sd,sa,height);
        e=length(sa,sc,height*(float)Math.sqrt(2));

        abc=area(a,b,e);
        adc=area(c,d,e);

        max=Float.max((float)Math.pow((abc+adc),2),max);
        min=Float.min((float)Math.pow((abc+adc),2),min);
        System.out.println((4*(int)max+" "+4*(int)min).toString());
        s.close();
    }

    
}

/*Input

10
5 5 5 5
1 1 1 1
U U D D

*/