package codevita;

import java.util.Scanner;

public class path_through_graph {

    static int largest_factor(int n)
    {
        int i;
        for (i = 2; i*i <=n; i++)
        {
            if (n % i == 0)
            {
                return n/i;
            }
        }
        return 1;
    }

    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        int a=0,b=0;
        if(s.hasNext())
            a=s.nextInt();
        if(s.hasNext())
            b=s.nextInt();
        if(a==b)
        {
            System.out.println(0);
            s.close();
            return;
        }
        int count=0;
        while(a!=b){
            while(a>b)
            {
                a=largest_factor(a);
                count++;
            }
            while(b>a)
            {
                b=largest_factor(b);
                count++;
            }
        }
        System.out.println(count);
        s.close();
    }
}