package codevita;

import java.util.Scanner;

public class virus {
    static String grid[]={
        "********************",
        "*....c.............*",
        "*...c..............*",
        "*c.................*",
        "*.............a....*",
        "*c.c...............*",
        "*.a................*",
        "*.............c....*",
        "********************",
    };
    static int inci=-1,incj=1;

    public static void resolve(int dir){
        switch(dir){
            case 0:
                inci=-1;
                incj=1;
                break;
            case 1:
                inci=-1;
                incj=-1;
                break;
            case 2:
                inci=1;
                incj=-1;
                break;
            case 3:
                inci=1;
                incj=1;
                break;
        }

    }

    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        for(int i=0;i<grid.length;i++)
            if(s.hasNext())
                grid[i]=s.nextLine();
        s.close();
        int i=8,j=0;
        
        int wallhitcount=0;
        int dir=0;
        while(wallhitcount<2){
            System.out.println(j+" "+(8-i));
            i+=inci;
            j+=incj;
            
            char ch=grid[i].charAt(j);
            if(ch=='.')
                continue;
            if(ch=='a'||ch=='*'){
                dir=(dir+1)%4;
                if(ch=='a')
                {
                    String update="";
                    for(int t=0;t<grid[i].length();t++)
                        if(t!=j)
                            update+=grid[i].charAt(t);
                        else
                            update+='-';
                    grid[i]=update;
                    resolve(dir);
                    continue;
                }
                if(ch=='*')
                {
                    wallhitcount++;
                    resolve(dir);
                    continue;
                }
            }
            else if(ch=='c'){
                dir=(dir+4-1)%4;
                String update="";
                for(int t=0;t<grid[i].length();t++)
                    if(t!=j)
                        update+=grid[i].charAt(t);
                    else
                        update+='-';
                grid[i]=update;
                resolve(dir);
                continue;
            } 
        }
        System.out.println(j+" "+(8-i));
        int inf=0,safe=0;
        for(int t=0;t<grid.length;t++)
        {
            System.out.println(grid[t]);
            for(int u=0;u<grid[i].length();u++){
                if(grid[t].charAt(u)=='a'||grid[t].charAt(u)=='c')
                    safe++;
                else if(grid[t].charAt(u)=='-')
                    inf++;
            }
        }
        System.out.println("safe="+safe);
        System.out.println("infected="+inf);

    }
    
}


