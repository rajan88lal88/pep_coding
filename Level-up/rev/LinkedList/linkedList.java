package LinkedList;




public class linkedList<K> {
    private class Node {
        K data = null;
        Node next = null;

        Node(K data) {
            this.data = data;
        }
    }

    private Node head = null;
    private Node tail = null;
    private int count = 0;

    public int size() {
        return this.count;
    }

    public boolean isEmpty() {
        return this.count == 0;
    }

    public Node getNodeAt(int idx) {
        if (idx < 0 || idx >= this.count) {
            throw new NullPointerException();
        }

        Node curr = this.head;
        while (idx-- > 0) {
            curr = curr.next;
        }

        return curr;
    }

    private void addFirstNode(Node node) {
        if (this.count == 0) {
            this.tail = node;
        }

        node.next = this.head;
        this.head = node;
        this.count++;
    }

    public void addFirst(K data) {
        Node node = new Node(data);
        addFirstNode(node);
    }

    private void addLastNode(Node node) {
        if (this.count == 0) {
            this.head = node;
            this.tail = node;
        } else {
            this.tail.next = node;
            this.tail = node;
        }

        this.count++;
    }

    public void addLast(K data) {
        Node node = new Node(data);
        addLastNode(node);
    }

    private void addAtNode(Node node, int idx) {
        if (idx == 0)
            addFirstNode(node);
        else if (idx == this.count)
            addLastNode(node);
        else {
            Node prev = getNodeAt(idx - 1);
            Node forw = prev.next;

            prev.next = node;
            node.next = forw;
        }
    }

    public void addAt(K data, int idx) {
        if (idx < 0 || idx > this.count)
            throw new NullPointerException();
        Node node = new Node(data);
        addAtNode(node, idx);
    }

    // get.=================================================

    public K getFirst() throws Exception {
        if (this.count == 0)
            throw new Exception("Empty List");
        return this.head.data;
    }

    public K getLast() throws Exception {
        if (this.count == 0)
            throw new Exception("Empty List");
        return this.tail.data;
    }

    public K getAt(int idx) {
        return getNodeAt(idx).data;
    }

    // remove==================================================

    private Node removeFirstNode() {
        Node rn = this.head;

        if (this.count == 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.head = this.head.next;
        }

        rn.next = null;
        this.count--;
        return rn;
    }

    public K removeFirst() throws Exception {
        if (this.count == 0)
            throw new Exception("Empty List");
        return removeFirstNode().data;
    }

    private Node removeLastNode() {
        Node rn = this.tail;

        if (this.count == 1) {
            this.head = null;
            this.tail = null;
        } else {
            Node secondLast = getNodeAt(this.count - 2);
            this.tail = secondLast;
            tail.next = null;
        }

        rn.next = null;
        this.count--;
        return rn;
    }

    public K removeLast() throws Exception {
        if (this.count == 0)
            throw new Exception("Empty List");
        return removeLastNode().data;
    }

    private Node removeAtNode(int idx) {
        if (idx == 0)
            return removeFirstNode();
        else if (idx == this.count - 1)
            return removeLastNode();
        else {
            // backup
            Node prev = getNodeAt(idx - 1);
            Node forw = prev.next.next;
            this.count--;
            Node rn = prev.next;

            rn.next = null;
            prev.next = forw;

            return rn;
        }
    }

    @Override
    public String toString() {
        String res = "[";
        for (Node start = head; start != null; start = start.next)
        {
            res += " "+start.data;
            if(start.next!=null)
                res+=",";
        }
        res += "]";
        return res;
    }

    public K removeAt(int idx) throws Exception {
        if (idx < 0 || idx >= this.count)
            throw new Exception("Null pointer");
        return removeAtNode(idx).data;
    }

}