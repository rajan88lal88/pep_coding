package LinkedList;
import java.util.HashMap;
public class LRU_cache {
    public static class LRU {
        private class Node{
            int key = 0;
            int value = 0;
    
            Node prev = null;
            Node next = null;
    
            Node(int key,int value){
                this.key = key;
                this.value = value;
            }
        }
    
        private Node head = null;
        private Node tail = null;
        private int maxSize = 0;
        private int size = 0;
    
        private HashMap<Integer,Node> map = new HashMap<>();

        public LRU(int capacity) {
            this.maxSize = capacity;
        }
        
        public int get(int key) {
            if(!map.containsKey(key)) return -1;

            Node node = map.get(key);
            removeNode(node);
            addLast(node); 
            return node.value;
        }
        
        public void put(int key, int value) {
            if(map.containsKey(key)){
                Node tmp = map.get(key);
                removeNode(tmp);
                addLast(tmp);
                if(tmp.value != value)
                   this.tail.value = value;
            }else{
                Node node = new Node(key,value);
                map.put(key,node);
                addLast(node);
    
                if(this.size > this.maxSize){
                    Node tmp=this.head;
                    removeNode(tmp);
                    map.remove(tmp.key);
                } 
            }
        }
    
        public void addLast(Node node){
            if(this.tail==null)
            {
                this.head=node;
                this.tail=node;
            }
            else
            {
                this.tail.next=node;
                node.prev=this.tail;
                this.tail=node;
                this.tail.next=null;
            } 
            this.size++;
        }
        public void removeNode(Node node){
            if(this.size==1)
            {
                this.head=null;
                this.tail=null;
            }
            else if(node==this.head){
                this.head=this.head.next;
                this.head.prev=null;
            }
            else if(node==this.tail){
                this.tail=this.tail.prev;
                this.tail.next=null;
            }
            else{
                node.prev.next=node.next;
                node.next.prev=node.prev;
                node.next=null;
                node.prev=null;
            }
            this.size--;
            
        }
    }
    public static void main(String[] args) {
        LRU cache=new LRU(2);
        cache.get(2);
        cache.put(2, 6);
        cache.get(1);
        cache.put(1, 5);
        cache.put(1, 2);
        cache.get(1);
        cache.get(2);
    }
}