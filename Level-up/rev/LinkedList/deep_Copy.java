package LinkedList;

/**
 * deep_Copy
 */
public class deep_Copy {

    public static class Node {
        int val;
        Node next;
        Node random;
    
        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }

    }
    public static Node deepCopy(Node head){
        if(head==null)
            return head;
        Node it=head;
        while(it!=null){
            Node nxt=it.next;
            it.next=new Node(it.val);
            it.next.next=nxt;
            it=nxt;
        }
        
        it=head;
        Node nh=null;
        Node t=null;
        while(it!=null){
            if(it.random!=null)
                it.next.random=it.random.next;
            it=it.next.next;
        }
        it=head;
        while(it!=null){
            if(nh==null){
                nh=it.next;
                t=it.next;
            }
            else
            {
                t.next=it.next;
                t=t.next;
            }
            it.next=it.next.next;
            it=it.next;
        }
        return nh;
    }
    public static void main(String[] args) {
        Node _1=new Node(7);
        Node _2=new Node(13);
        Node _3=new Node(11);
        Node _4=new Node(10);
        Node _5=new Node(1);
        _1.next=_2;
        _2.next=_3;
        _3.next=_4;
        _4.next=_5;
        _1.random=null;
        _2.random=_1;
        _3.random=_5;
        _4.random=_3;
        _5.random=_1;
        Node nh=deepCopy(_1);

        while(nh!=null){
            System.out.println(nh.val+"\t"+nh+"\t"+nh.next+"\t"+nh.random);
            nh=nh.next;
        }
        Node h=_1;
        System.out.println();
        while(h!=null){
            System.out.println(h.val+"\t"+h+"\t"+h.next+"\t"+h.random);
            h=h.next;
        }
    }
}