package LinkedList;
import java.util.*;
public class gfg_odd_even {
    
    
    public static class Node {
        int data = 0;
        Node next = null;

        Node(int data) {
            this.data = data;
        }
    }
    // @Override
    public static String toString(Node head) {
        String res = "[";
        for (Node start = head; start != null; start = start.next)
        {
            res += " "+start.data;
            if(start.next!=null)
                res+=",";
        }
        res += "]";
        return res;
    }
    public static Node oddeven(Node head){
        Node even=null;
        Node odd=null;
        Node ite=null;
        Node ito=null;
        while(head!=null){
            if(head.data%2==0)
            {
                if(even==null)
                {
                    even=head;
                    ite=even;
                }
                else
                {
                    ite.next=head;
                    ite=ite.next;
                }
            }
            else
            {
                if(odd==null)
                {
                    odd=head;
                    ito=odd;
                }
                else
                {
                    ito.next=head;
                    ito=ito.next;
                }
            }
            head=head.next;
        }
        ite.next=odd;
        return even;
    }
    public static void main(String[] args) {
        Node head=new Node(17);
        Node it=head;
        int arr[]={15,8,9,2,4,1};
        for(int i=0;i<arr.length;i++)
        {
            it.next=new Node(arr[i]);
            it=it.next;
        }
        System.out.println(toString(head));
        head=oddeven(head);
        System.out.println(toString(head));
    }
}