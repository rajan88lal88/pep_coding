public class temp {

    public static double[] findroots(double input1, double input2, double input3) 
    { 
     
        double a=input1;
        double b=input2;
        double c=input3;
        double d  = b*b - 4*a*c; 
        double sqrt_val = Math.sqrt(Math.abs(d)); 
        double res[]=new double[2];
        if (d > 0) 
        { 
            res[0]=(double)(-b + sqrt_val) / (2 * a);
            res[1]=(double)(-b - sqrt_val) / (2 * a); 
        } 
        else 
        { 
            res[0]=-(double)b / ( 2 * a ) + sqrt_val;
            res[1]=-(double)b / ( 2 * a ) + sqrt_val;
        } 
        return res;
    }

    public static void main(String[] args) {
        double[]res=findroots(1, -2, -3);
        System.out.println(res[0]+":"+res[1]);
    }
}