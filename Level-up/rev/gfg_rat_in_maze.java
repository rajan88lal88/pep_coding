// { Driver Code Starts
// Initial Template for Java

import java.util.*;
class Rat {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        while (t-- > 0) {
            int n = sc.nextInt();
            int[][] a = new int[n][n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++) a[i][j] = sc.nextInt();

            // GfG g = new GfG();
            ArrayList<String> res = GfG.printPath(a, n);
            if (res.size() > 0) {
                for (int i = 0; i < res.size(); i++)
                    System.out.print(res.get(i) + " ");
                System.out.println();
            } else {
                System.out.println(-1);
            }
        }
        sc.close();
    }
}
// } Driver Code Ends

/*

3
4
1 0 0 0 1 1 0 1 0 1 0 0 0 1 1 1
4
1 0 0 0 1 1 0 1 1 1 0 0 0 1 1 1
2
1 0 1 0 


*/
// User function Template for Java

// m is the given matrix and n is the order of matrix
class GfG {
    static int[][] dir={
        {-1,0},
        {1,0},
        {0,-1},
        {0,1}
    };
    static String [] name={
        "U","D","L","R"
    };
    static ArrayList<String> res;
    public static void paths(boolean [][]vis,int si,int sj,int ei,int ej,String path,int[][]m){
        
        if(si>ei||sj>ej||si<0||sj<0||vis[si][sj]==true||m[si][sj]==0)
            return;
        if(si==ei&&sj==ej)
        {
            // System.out.println(path);
            res.add(path);
            return;
        }    
        
        vis[si][sj]=true;
        for(int i=0;i<dir.length;i++){
            paths(vis,si+dir[i][0],sj+dir[i][1],ei,ej,path+name[i],m);
        }
        vis[si][sj]=false;
    }
    
    public static ArrayList<String> printPath(int[][] m, int n) {
        // Your code here
        res=new ArrayList<>();
        paths(new boolean[n][n],0,0,n-1,n-1,"",m);
        Collections.sort(res);
        return res;
        
    }
}