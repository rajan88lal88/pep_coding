package Trees;

public class pred_succ {

    public static class Node{
        int data = 0;
        Node left = null;
        Node right = null;

        Node(int data){
            this.data = data;
        }
    }

    static int idx = 0;
    public static Node constructTree(int[] arr){
        if(idx==arr.length || arr[idx]==-1){
            idx++;
            return null;
        }

        Node node=new Node(arr[idx++]);

        node.left = constructTree(arr);
        node.right = constructTree(arr);

        return node;
    }

    public static void display(Node node){
        if(node==null) return;
        StringBuilder sb = new StringBuilder();
        
        sb.append(node.left!=null?node.left.data+"" : ".");
        sb.append(" <- " + node.data + " -> ");
        sb.append(node.right!=null?node.right.data+"" : ".");

        System.out.println(sb.toString());

        display(node.left);
        display(node.right);
    }

    public static int size(Node node){
        return node==null? 0 : size(node.left) + size(node.right) + 1; 
    }

    public static int height(Node node){
        return node==null? -1 : Math.max(height(node.left) , height(node.right)) + 1; 
    }


    public static class pair{
        int height=0;
        int size=0;
        boolean find=false;
        int ceil=(int)1e8;
        int floor=-(int)1e8;
        Node pred=null,succ=null,prev=null;
    }

    public static void findPredSucc(Node root,int level, int data, pair ans){
        if(root==null) return;
        //additional tasks optional
        ans.height=Integer.max(level,ans.height);
        ans.size++;
        ans.find=ans.find||root.data==data;
        //setting prev,pred,succ
        if(root.data>data) ans.ceil=Integer.min(ans.ceil,root.data);
        if(root.data<data) ans.floor=Integer.max(ans.floor,root.data);
        if(root.data==data&&ans.pred==null) ans.pred=ans.prev;
        if(ans.prev!=null&&ans.prev.data==data) ans.succ=root;
        ans.prev=root;

        findPredSucc(root.left, level+1, data, ans);
        findPredSucc(root.right, level+1, data, ans);


        
    }

    public static void main(String[] args) {
        int[] arr={1,2,4,8,-1,-1,9,-1,-1,5,10,-1,-1,11,-1,-1,3,6,-1,13,-1,-1,7,14,-1,-1,-1};
        Node root = constructTree(arr);
        display(root);
        pair res=new pair();
        

        findPredSucc(root, 0, 2, res);
        System.out.println("pred  : "+res.pred.data);
        System.out.println("succ  : "+res.succ.data);
        System.out.println("ceil  : "+res.ceil);
        System.out.println("floor  : "+res.floor);

    }
    
}