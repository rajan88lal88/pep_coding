package Trees;
public class pred_succ_BST {

    public static class Node {
        int data = 0;
        Node left = null;
        Node right = null;

        Node(int data) {
            this.data = data;
        }
    }

    static int idx = 0;

    public static Node constructTree(int[] arr) {
        if (idx == arr.length || arr[idx] == -1) {
            idx++;
            return null;
        }

        Node node = new Node(arr[idx++]);

        node.left = constructTree(arr);
        node.right = constructTree(arr);

        return node;
    }

    public static void display(Node node) {
        if (node == null)
            return;
        StringBuilder sb = new StringBuilder();

        sb.append(node.left != null ? node.left.data + "" : ".");
        sb.append(" <- " + node.data + " -> ");
        sb.append(node.right != null ? node.right.data + "" : ".");

        System.out.println(sb.toString());

        display(node.left);
        display(node.right);
    }

    public static int size(Node node) {
        return node == null ? 0 : size(node.left) + size(node.right) + 1;
    }

    public static int height(Node node) {
        return node == null ? -1 : Math.max(height(node.left), height(node.right)) + 1;
    }
    public static int minimum(Node node){
        Node curr = node;

        while(curr.left != null)
            curr=curr.left;

        return curr.data;
    }

    public static int maximum(Node node){
        Node curr = node;

        while(curr.right != null)
            curr=curr.right;

        return curr.data;
    }
    public static Node removeData(Node node,int data){
        if(node == null) return null;

        if(data < node.data) node.left = removeData(node.left,data);
        else if(data < node.data) node.right = removeData(node.right,data);
        else{
            if(node.left == null || node.right == null) return  node.left != null? node.left: node.right;

            int minEle = minimum(node.right);
            node.data = minEle;

            node.right = removeData(node.right,minEle);
        }

        return node;
    }


    public static void findPredSucc(Node root, int data) {
        Node curr = root, pred = null, succ = null;
        while (curr != null) {
            if(pred!=null)
                System.out.print("Pred : "+pred.data);
            else
                System.out.print("Pred : "+(-1));

            System.out.print("\tCurr :"+ curr.data);


            if(succ!=null)
                System.out.println("\tSucc : "+succ.data);
            else
                System.out.println("\tSucc : "+(-1));

            if (curr.data == data) {
                if (curr.left != null) {
                    pred = curr.left;
                    while (pred.right != null)
                        pred = pred.right;
                }
                if (curr.right != null) {
                    succ = curr.right;
                    while (succ.left != null)
                        succ = succ.left;
                }
                break;
            }

            if (data < curr.data) {
                succ = curr;
                curr = curr.left;
            } else {
                pred = curr;
                curr = curr.right;
            }
        }
        System.out.println("Pred  :  " + pred.data);
        System.out.println("Succ  :  " + succ.data);
    }

    public static void main(String[] args) {
        int[] arr = { 60, 41, 16, -1, 25, -1, -1, 53, 46, 42, -1, -1,-1, 55, -1, -1, 74, 65, 63, 62, -1, -1, 64, -1, -1,
                70, -1, -1, -1 };
        Node root = constructTree(arr);
        display(root);
        findPredSucc(root, 55);
    }

}