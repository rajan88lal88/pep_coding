package Trees;

public class avl {

    public static class Node{
        int data = 0;
        Node left = null;
        Node right = null;
        int height=0;
        int bal=0;
        Node(int data){
            this.data = data;
        }
    }
    public static int minimum(Node node){
        Node curr = node;

        while(curr.left != null)
            curr=curr.left;

        return curr.data;
    }

    public static int maximum(Node node){
        Node curr = node;

        while(curr.right != null)
            curr=curr.right;

        return curr.data;
    }
    public static void display(Node node){
        if(node==null) return;
        StringBuilder sb = new StringBuilder();
        
        sb.append(node.left!=null?node.left.data+"" : ".");
        sb.append(" <- " + node.data + " -> ");
        sb.append(node.right!=null?node.right.data+"" : ".");

        System.out.println(sb.toString());

        display(node.left);
        display(node.right);
    }

    public static void updateHeightBal(Node root) {
        
        
        int lh=root.left==null?-1:root.left.height;
        int rh=root.right==null?-1:root.right.height;
        root.height=Integer.max(lh,rh)+1;
        root.bal=lh-rh;
        
    }

    public static Node rightRotation(Node A) {
        Node B=A.left;
        Node Br=B.right;
        B.right=A;
        A.left=Br;
        updateHeightBal(A);
        updateHeightBal(B);
        return B;
    }

    public static Node leftRotation(Node A) {
        Node B=A.right;
        Node Bl=B.left;
        B.left=A;
        A.right=Bl;
        updateHeightBal(A);
        updateHeightBal(B);
        return B;
    }

    
    

    public static Node getRotation(Node root){
        updateHeightBal(root);
        if(root.bal==2){//ll,lr
            if(root.left.bal==1)//ll
                root=rightRotation(root);
            else//lr
            {
                root=leftRotation(root.left);
                root=rightRotation(root);
            } 
                
        }

        if(root.bal==-2){//rr,rl
            if(root.right.bal==-1)//rr
                root=leftRotation(root);
            else//rl
            {
                root=rightRotation(root.right);
                root=leftRotation(root);
            }
        }
        return root;

    }

    public static Node addNode(Node node,int data){
        if(node == null) return new Node(data);
        if(data < node.data) node.left = addNode(node.left,data);
        else node.right = addNode(node.right,data);
        node=getRotation(node);
        return node;
    }

    public static Node removeData(Node node,int data){
        if(node == null) return null;

        if(data < node.data) node.left = removeData(node.left,data);
        else if(data < node.data) node.right = removeData(node.right,data);
        else{
            if(node.left == null || node.right == null) return  node.left != null? node.left: node.right;

            int minEle = minimum(node.right);
            node.data = minEle;

            node.right = removeData(node.right,minEle);
        }
        node=getRotation(node);
        return node;
    }
    public static void main(String[] args) {
        Node root=null;
        for(int i=1;i<=20;i++)
            root=addNode(root, i*10);
        System.out.println(root.bal+":"+root.height);
        display(root);
    }

    
}