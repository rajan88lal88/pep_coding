package Trees;
import java.util.*;
public class burn_binary_tree {

    public static class Node{
        int data = 0;
        Node left = null;
        Node right = null;

        Node(int data){
            this.data = data;
        }
    }

    static int idx = 0;
    public static Node constructTree(int[] arr){
        if(idx==arr.length || arr[idx]==-1){
            idx++;
            return null;
        }

        Node node=new Node(arr[idx++]);

        node.left = constructTree(arr);
        node.right = constructTree(arr);

        return node;
    }

    public static void display(Node node){
        if(node==null) return;
        StringBuilder sb = new StringBuilder();
        
        sb.append(node.left!=null?node.left.data+"" : ".");
        sb.append(" <- " + node.data + " -> ");
        sb.append(node.right!=null?node.right.data+"" : ".");

        System.out.println(sb.toString());

        display(node.left);
        display(node.right);
    }

    public static int size(Node node){
        return node==null? 0 : size(node.left) + size(node.right) + 1; 
    }

    public static int height(Node node){
        return node==null? -1 : Math.max(height(node.left) , height(node.right)) + 1; 
    }


    // public static int  burn(Node root,int val, List<List<Integer>> ans){
    //     if(root==null)
    //         return -1;
                
    // }
    // https://www.geeksforgeeks.org/burn-the-binary-tree-starting-from-the-target-node/

    public static void BurningNodes(Node node,int time,ArrayList<ArrayList<Integer>> ans){
        if(node == null) return;

        if(time == ans.size()) ans.add(new ArrayList<>());
        ans.get(time).add(node.data);

        BurningNodes(node.left, time + 1, ans);
        BurningNodes(node.right, time + 1, ans);
    }

    public static int burningOfTree_(Node node,int data,ArrayList<ArrayList<Integer>> ans)
    {
        if(node==null) return -1;

        if(node.data == data){
            BurningNodes(node,0,ans);
            return 1;
        }

        int ld = burningOfTree_(node.left, data, ans);
        if(ld != -1){
        
            if(ld == ans.size()) ans.add(new ArrayList<>());    
            ans.get(ld).add(node.data);
            BurningNodes(node.right,ld + 1,ans);
            return ld + 1;
        
        }

        int rd = burningOfTree_(node.right, data, ans);
        if(rd != -1){

            if(rd == ans.size()) ans.add(new ArrayList<>());    
            ans.get(rd).add(node.data);
            BurningNodes(node.left,rd + 1,ans);
            return rd + 1;
        
        }

        return -1;
    }
    public static void main(String[] args) {
        int[] arr={12,13,-1,-1,10,14,21,-1,-1,24,-1,-1,15,22,-1,-1,23,-1,-1};
        Node root = constructTree(arr);
        display(root);
        ArrayList<ArrayList<Integer>> res=new ArrayList<>();

        burningOfTree_(root,14,res);
        System.out.print(res.toString());

    }
}