package Trees;

import java.util.ArrayList;
import java.util.Stack;

public class generic_tree {
    public static class Node{
        int data = 0;
        ArrayList<Node> Children=new ArrayList<>();

        Node(int data){
            this.data = data;
        }
    }

    public static Node constructGT(int arr[]) {
        Stack<Node> st=new Stack<>();
        for(int x:arr){
            if(x!=-1)
                st.add(new Node(x));
            else
            {
                Node tmp=st.pop();
                if(st.size()==0)
                    return tmp;
                st.peek().Children.add(tmp);
            }
        }
        return st.peek();
    }

    public static int height(Node root) {
        int max=0;
        for(Node e:root.Children){
            max=Integer.max(height(e),max);
        }
        return max+1;
    }

    public static void display(Node root) {
        System.out.print(root.data +" --> ");
        for(Node n:root.Children){
            System.out.print(n.data+", ");
        }
        System.out.println();
        for(Node n:root.Children){
            display(n);
        }
    }
    public static int size(Node root){
        int size=0;
        for(Node e:root.Children)
        {
            size+=size(e);
        }
        return size+1;
    }
    public static int maximum(Node root) {
        int max=root.data;
        for(Node e:root.Children){
            max=Integer.max(maximum(e),max);
        }
        return max;
        
    }
    public static boolean find(Node root,int data) {
        if(root.data==data)
            return true;
        boolean res=false;
        for(Node e:root.Children){
            res= res||find(e,data);
        }
        return res;
    }

    public static boolean NodeToRootPath(Node node,int data,ArrayList<Node> path){
        if(node.data == data){
            path.add(node);
            return true;
        }

        boolean res = false;
        for(Node child : node.Children){
            res = res || NodeToRootPath(child,data,path);
        }

        if(res) path.add(node);

        return res;
    }

    public static Node LCA(Node root,int p,int q) {
        if(root.data==p||root.data==q)
            return root;
        ArrayList<Node> res=new ArrayList<>();
        for(Node e:root.Children){
            Node tmp=LCA(e, p, q);
            if(tmp!=null)
                res.add(tmp);
        }
        if(res.size()==2)
            return root;
        if(res.size()==1)
            return res.get(0);
        else
            return null;

    }
    public static void zig_Zag(Node node){

        Stack<Node> st1= new Stack<>();
        Stack<Node> st2= new Stack<>();

        st1.push(node);
        boolean flag = true;
        while(st1.size()!=0){
            int size = st1.size();
            while(size-->0){
                
                Node rnode = st1.pop();
                System.out.print(rnode.data+" ");
            
                if(flag){
                   for(int i=0;i<rnode.Children.size();i++){
                      Node child  = rnode.Children.get(i);
                      st2.push(child);
                    }
                }else{
                    for(int i=rnode.Children.size()-1;i>=0;i--){
                       Node child  = rnode.Children.get(i);
                       st2.push(child);
                    }
                }
            }

            flag = !flag;
            Stack<Node> temp = st1;
            st1 = st2;
            st2 = temp;
            System.out.println();
            
        }
    }
    public static Node linearize(Node node){
        if(node.Children.size() == 0) return node;

        int n = node.Children.size();
        Node oTail = linearize(node.Children.get(n - 1));
        for(int i = n-2;i>=0;i--){
            Node tail = linearize(node.Children.get(i));

            tail.Children.add(node.Children.get(i + 1));
            node.Children.remove(node.Children.size()-1);
        }

        return oTail;
    }
    public static void main(String[] args) {
        int arr[]={10, 20, 50, -1, 60, -1, -1, 30, 70, -1, 80, 100, -1, 110, -1, -1, 90, -1, -1, 40, 120, 140, -1, 150, -1, -1, -1,-1};
        Node root=constructGT(arr);
        display(root);
        System.out.println("Height : "+ height(root));
        System.out.println("Size : "+ size(root));
        System.out.println("Max : "+ maximum(root));
        System.out.println("Find 15: "+ find(root,15));
        System.out.println("Find 80: "+ find(root,80));
        System.out.println("LCA 110 70 : "+ LCA(root,110,70).data);
    }
    
}