import java.util.*;

/**
 * GFG_Rat_in_Maze_with_multiple_steps
 Input : { {2, 1, 0, 0},
         {3, 0, 0, 1},
         {0, 1, 0, 1},
          {0, 0, 0, 1}
        }
Output : { {1, 0, 0, 0},
           {1, 0, 0, 1},
           {0, 0, 0, 1},
           {0, 0, 0, 1}
         }

Explanation 
Rat started with M[0][0] and can jump upto 2 steps right/down. 
Let's try in horizontal direction - 
M[0][1] won't lead to solution and M[0][2] is 0 which is dead end. 
So, backtrack and try in down direction. 
Rat jump down to M[1][0] which eventually leads to solution.  

Input : { 
      {2, 1, 0, 0},
      {2, 0, 0, 1},
      {0, 1, 0, 1},
      {0, 0, 0, 1}
    }
Output : Solution doesn't exist
 
 */


public class GFG_Rat_in_Maze_with_multiple_steps {


    public static boolean solve_multi_jump(int [][]maze,int sol[][],int si,int sj, int ei,int ej, boolean vis[][]){
        if(si>ei||sj>ej||vis[si][sj]==true)
            return false;
        if(si==ei&&sj==ej){
            // System.out.println("path found");
            sol[si][sj]=1;
            return true;
        }
        vis[si][sj]=true;
        boolean res=false;
        for(int i=1;i<=maze[si][sj];i++)
        {
            res=res||solve_multi_jump(maze, sol, si, sj+i, ei, ej, vis);
            res=res||solve_multi_jump(maze, sol, si+i, sj, ei, ej, vis);
        }
        vis[si][sj]=false;
        if(res)
            sol[si][sj]=1;
        // System.out.println("path found");
        return res;
    }
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        int n=s.nextInt();
        int maze[][]=new int[n][n];
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++)
                maze[i][j]=s.nextInt();
        int sol[][]=new int[n][n];
        solve_multi_jump(maze,sol,0,0,n-1,n-1,new boolean[n][n]);
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++)
                System.out.print(sol[i][j]+"  ");
            System.out.println();
        }
        s.close();
    }
}