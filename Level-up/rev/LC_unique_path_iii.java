// import java.util.*;

class LC_unique_path_iii {

    public static int solve(int dir[][],int grid[][],boolean vis[][],int si,int sj,int ei,int ej,int curr, int tar){
        if(si<0||sj<0||si>=grid.length||sj>=grid[0].length||vis[si][sj]==true||grid[si][sj]==-1)
        {
            // System.out.println(si+":"+sj);
            return 0;
        }
        if(si==ei&&sj==ej)
            if(curr==tar)
                return 1;
            else    
                return 0;
        int count=0;
        vis[si][sj]=true;
        for(int i=0;i<dir.length;i++){
           
            count+=solve(dir,grid,vis,si+dir[i][0],sj+dir[i][1],ei,ej,curr+1,tar);
        }
        vis[si][sj]=false;
        return count;
    }
    
    public static int uniquePathsIII(int[][] grid) {
        int m=grid.length;
        int n=grid[0].length;
        int si=0,sj=0,ei=0,ej=0,total=0;
        for(int i=0;i<m;i++)
            for(int j=0;j<n;j++)
            {
                if(grid[i][j]==1)
                {
                    si=i;
                    sj=j;
                }
                else if(grid[i][j]==2)
                {
                    ei=i;
                    ej=j;
                }
                else if(grid[i][j]==0)
                    total++;
            }
        // System.out.println(total);
        int dir[][]={
            {1,0},
            {0,1},
            {-1,0},
            {0,-1}
        };
        return solve(dir,grid,new boolean[m][n],si,sj,ei,ej,-1,total);
    }
    public static void main(String[] args) {
        int arr[][]={
            {1,0,0,0},
            {0,0,0,0},
            {0,0,0,2}
        };
        System.out.println(uniquePathsIII(arr));
    }
}