import java.util.*;

public class sudoku_solve {
    public static boolean checkplacement(int[][]board,int row, int col,int num){
        int r=(row/3)*3;
        int c=(col/3)*3;
        for(int i=r;i<r+3;i++){
            for(int j=c;j<c+3;j++){
                if(board[i][j]==num)
                    return false;
            }
        }
        for(int i=0;i<9;i++){
            if(board[i][col]==num)
                return false;
        }
        
        for(int i=0;i<9;i++){
            if(board[row][i]==num)
                return false;
        }
        return true;
    }
    public static int[][] res=new int[9][9];
    public static boolean placedigit(int[][]board,int idx,ArrayList<Integer> loz)
    {
        if(idx==loz.size())
        {
            for(int i=0;i<9;i++)
                for(int j=0;j<0;j++)
                    res[i][j]=board[i][j];
            return true;
        }
            
        int row=loz.get(idx)/9;
        int col=loz.get(idx)%9;
        for(int i=1;i<=9;i++){
            if(checkplacement(board,row,col,i)==true)
            {
                board[row][col]=i;
                if(placedigit(board,idx+1,loz)==true)
                    return true;
                board[row][col]=0;
            }
        }
        return false;
    }
    
    public static void solveSudoku(char[][] board) {
        int b[][]=new int[9][9];
        ArrayList<Integer> loz=new ArrayList<>();
        for(int i=0;i<9;i++)
            for(int j=0;j<9;j++)
                if(board[i][j]!='.')
                    b[i][j]=(int)(board[i][j]-'0');
                else
                    loz.add(i*9+j);
                    
        placedigit(b,0,loz);
        for(int i=0;i<9;i++)
            for(int j=0;j<9;j++)
                board[i][j]=(char)('0'+b[i][j]);
    }

    public static void main(String[] args) {
        char[][]board={
            {'5','3','.','.','7','.','.','.','.'},
            {'6','.','.','1','9','5','.','.','.'},
            {'.','9','8','.','.','.','.','6','.'},
            {'8','.','.','.','6','.','.','.','3'},
            {'4','.','.','8','.','3','.','.','1'},
            {'7','.','.','.','2','.','.','.','6'},
            {'.','6','.','.','.','.','2','8','.'},
            {'.','.','.','4','1','9','.','.','5'},
            {'.','.','.','.','8','.','.','7','9'}
        };
        solveSudoku(board);
        for(int i=0;i<9;i++)
        {
            for(int j=0;j<9;j++)
                System.out.print(board[i][j]+"\t");
            System.out.println();
        }

    }
    
}