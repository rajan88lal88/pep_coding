package temp;

public class maximum_water {

    public static void main(String[] args) {
        int height[]={1,8,6,2,5,4,8,3,7};
        int max=0;
        for(int i=0,j=height.length-1;i<j;){
            max=Integer.max(max,(j-i)*Integer.min(height[i],height[j]));
            System.out.println(max);
            if(height[i]<height[j])
                i++;
            else
                j--;
        }
        System.out.println(max);
    }
    
}
