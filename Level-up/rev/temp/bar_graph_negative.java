package temp;

public class bar_graph_negative {
    public static void print_bar(int arr[]) {
        int max = arr[0], min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min)
                min = arr[i];
            if (arr[i] > max)
                max = arr[i];
        }
        int lowerbound = 0;
        if (min < 0)
            lowerbound = min;
        System.out.println("lb : " + lowerbound);
        for (int i = max; i > lowerbound; i--) {
            for (int j = 0; j < arr.length; j++) {
                if (i > 0) {
                    if (arr[j] >= 0 && arr[j] >= i)
                        System.out.print("* ");
                    if (arr[j] >= 0 && arr[j] < i || arr[j] < 0)
                        System.out.print("  ");
                } else {
                    if (arr[j] + 1 <= i && arr[j] < 0)
                        System.out.print("* ");
                    else
                        System.out.print("  ");
                }

            }
            System.out.println();
        }

    }

    public static void main(String[] args) {
        int arr[] = { 1, 2, -1, 4, -5, 7, -2, 4, 3, 5, -1, -1, 6, -4 };
        print_bar(arr);
    }
}