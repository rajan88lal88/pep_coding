package temp;

public class dragon_princess {

    
    public static int steps(int input1,int input2,int input3[][]){
        boolean vis[][]=new boolean[input1][input2];
        findsteps(input1-1,0,input3,vis,0);
        if(min==Integer.MAX_VALUE)
            return -1;
        else
            return min;
    }


    static int min=Integer.MAX_VALUE;
    public static void findsteps(int i,int j, int arr[][],boolean vis[][],int steps){
        if(i<0||i>=arr.length||j<0||j>=arr[0].length||vis[i][j]==true||arr[i][j]==2)
            return;
        if(arr[i][j]==3){
            System.out.println(steps);
            if(isprime(steps))
            {
                min=Integer.min(min,steps);
            }    
            return;
        }
        vis[i][j]=true;
        findsteps(i+1, j, arr, vis, steps+1);
        findsteps(i-1, j, arr, vis, steps+1);
        findsteps(i, j+1, arr, vis, steps+1);
        findsteps(i, j-1, arr, vis, steps+1);
        vis[i][j]=false;
    }

    public static boolean isprime(int n){
        if (n <= 1) 
        return false; 
        for (int i = 2; i*i <= n; i++) 
            if (n % i == 0) 
                return false; 
        return true; 
    }
    public static void main(String[] args) {
        int arr[][]={
            {1,2,1,1,2,1},
            {2,1,2,1,1,0},
            {1,1,3,1,2,1},
            {1,2,1,0,1,2},
            {0,1,2,1,2,1},
            {0,0,1,0,1,0}
        };
        System.out.println(steps(6,6,arr));
    }
    
}
