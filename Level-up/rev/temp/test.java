package temp;

import java.util.*;

/**
 * test
 */
public class test {

    public static boolean checkZero(int arr[]) {
        for (int i = 0; i < arr.length; i++)
            if (arr[i] != 0)
                return false;
        return true;
    }

    public static void dec(int idx, int inc, int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            if (i != idx) {
                arr[i] -= inc;
                if (arr[i] < 0)
                    arr[i] = 0;
            }
        }
    }

    public static int findmax(int arr[]) {
        int max = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i] > arr[max])
                max = i;
        return max;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int p = s.nextInt();
        int q = s.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++)
            arr[i] = s.nextInt();
        int res;
        for (res = 0;; res++) {
            if (checkZero(arr))
                break;
            int i = findmax(arr);

            arr[i] -= p;
            if (arr[i] < 0)
                arr[i] = 0;
            dec(i, q, arr);
        }
        System.out.println(res);

    }
}

// 6 4 1
// 2 3 0
// 1 0 0
// 0 0 0