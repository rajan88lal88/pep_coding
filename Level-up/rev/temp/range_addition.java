package temp;

public class range_addition {
    public static int[] getModifiedArray(int length, int[][] updates) {
        // Write your code here
        int arr[]=new int[length];
        for(int i=0;i<updates.length;i++){
            print(arr);
            arr[updates[i][0]]+=updates[i][2];
            if(updates[i][1]+1<length)
                arr[updates[i][1]+1]-=updates[i][2];
        }
        print(arr);
        for(int i=1;i<arr.length;i++)
            arr[i]+=arr[i-1];
        return arr;
        
    }
    public static void print(int arr[]) {
        for(int i=0;i<arr.length;i++)
            System.out.print(arr[i]+" ");
        System.out.println();
    }

    public static void main(String[] args) {
        int arr[][]={{1,3,2},{2,4,3},{0,2,-2}};
        int res[]=getModifiedArray(5, arr);
        print(res);
    }
    
}
