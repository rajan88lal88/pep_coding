/**
 * permutation
 */
public class permutation {

    public static int permute(String str,String res){
        if(str.length()==0)
        {
            System.out.println(res);
            return 1;
        }
        int count=0;
        for(int i=0;i<str.length();i++){
            count+=permute(str.substring(0,i)+str.substring(i+1),res+str.charAt(i));
        }
        return count;
    }
    public static void main(String[] args) {
        System.out.println(permute("AACD",""));
    }
}