package String_Arrays;
import java.util.*;

//leetcode 632

public class smallest_range_k_ele {
    public class customcompare implements Comparator<List<Integer>>{
        public int compare(List<Integer> l1, List<Integer> l2) 
        { 
            return l1.get(0)-l2.get(0);
        }
    }
    public int[] smallestRange(List<List<Integer>> nums) {
        PriorityQueue<List<Integer>> pq=new PriorityQueue<>(new customcompare());
        int max=Integer.MIN_VALUE;
        for(int i=0;i<nums.size();i++)
        {
            List<Integer> ls=new ArrayList<>();
            ls.add(nums.get(i).get(0));
            if(nums.get(i).get(0)>max)
                max=nums.get(i).get(0);
            ls.add(i);
            ls.add(0);
            pq.add(ls);
        }
        int range=max-pq.peek().get(0);
        int low=pq.peek().get(0);
        int high=max;
        while(true){
            List<Integer> temp=pq.poll();
            if(max-temp.get(0)<range)
            {
                range=max-temp.get(0);
                low=temp.get(0);
                high=max;
            }
            int i=temp.get(1);
            int j=temp.get(2);
            if(j+1>=nums.get(i).size())
                break;
            int ele=nums.get(i).get(j+1);
            List<Integer> ls=new ArrayList<>();
            ls.add(ele);
            if(ele>max)
                max=ele;
            ls.add(i);
            ls.add(j+1);
            pq.add(ls);
        }
        int res[]=new int[2];
        res[0]=low;
        res[1]=high;
        return res;
    }
    public static void main(String[] args) {
        
    }
    
}
