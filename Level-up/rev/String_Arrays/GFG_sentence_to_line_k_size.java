package String_Arrays;

/**
 * temp
 */
public class GFG_sentence_to_line_k_size {
    public static void print(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
        System.out.println();
    }

    public static void solve(int arr[], int n, int k) {
        int dp[] = new int[n];
        int ans[] = new int[n];
        dp[n - 1] = 0;
        ans[n - 1] = n - 1;
        int cost = 0;
        for (int i = n - 2; i >= 0; i--) {
            print(dp);
            print(ans);
            System.out.println();
            dp[i] = Integer.MAX_VALUE;
            int curlen = -1;
            for (int j = i; j < n; j++) {
                curlen += arr[j] + 1;
                if (curlen > k)
                    break;
                if (j == n - 1)
                    cost = 0;
                else
                    cost = (k - curlen) * (k - curlen) + dp[j + 1];

                if (cost < dp[i]) {
                    dp[i] = cost;
                    ans[i] = j;
                }
            }
        }
        print(dp);
        print(ans);
        System.out.println();
        for (int i = 0; i < n;) {
            System.out.print((i + 1) + " " + (ans[i] + 1) + " ");
            i = ans[i] + 1;
        }
    }

    public static void main(String[] args) {
        int arr[] = { 5, 3, 5, 8, 4, 4, 7 };
        solve(arr, 7, 15);
    }
}
// “Geeks for Geeks presents word wrap problem”

// Geeks for Geeks
// presents word
// wrap problem

// The total extra spaces in line 1 and line 2 are 0 and 2.
// Space for line 3 is not considered as it is not extra space as described
// above.
// So optimal value of total cost is 0 + 2*2 = 4.