/**
 * permutation_unique
 */
public class permutation_unique {

    public static int unique_permute(String str,String res){
        boolean []check=new boolean[26];
        if(str.length()==0)
        {
            System.out.println(res);
            return 1;
        }

        int count=0;
        for(int i=0;i<str.length();i++){
            if(check[str.charAt(i)-'A']==false)
            {
                count+=unique_permute(str.substring(0,i)+str.substring(i+1), res+str.charAt(i));
                check[str.charAt(i)-'A']=true;
            }
        }
        return count;
    }
    public static void main(String[] args) {
        System.out.println(unique_permute("ABAB",""));
    }
}