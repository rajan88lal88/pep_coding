import java.io.*;
import java.util.*;

public class datesort {

  public static void sortDates(int[] arr) {
    //write your code here
    // int max=99999999;
    
    int exp[]={1000000,10000000,10000,100000,1,10,100,1000};
    for(int i=0;i<exp.length;i++)
    {
        countSort(arr,exp[i]);
    }

  }

  public static void countSort(int[] arr, int exp) {
    //write your code here
    int freq[]=new int[10];
    for(int val:arr){
        freq[(val/exp)%10]++;
    }
    for(int i=1;i<freq.length;i++)
        freq[i]+=freq[i-1];
        
    int ans[]=new int[arr.length];
    
    for(int i=arr.length-1;i>=0;i--){
        int val=(arr[i]/exp)%10;
        int idx=freq[val];
        ans[idx-1]=arr[i];
        freq[val]--;
    }
    
    for(int i=0;i<arr.length;i++){
        arr[i]=ans[i];
    }
  }

  public static void print(int[] arr) {
    for (int i = 0; i < arr.length; i++) {
      System.out.println(arr[i]);
    }
  }

  public static void main(String[] args) throws Exception {
    Scanner scn = new Scanner(System.in);
    int n = scn.nextInt();
    int[] arr = new int[n];
    for (int i = 0; i < n; i++) {
      String str = scn.next();
      arr[i] = Integer.parseInt(str, 10);
    }
    sortDates(arr);
    print(arr);
  }

}