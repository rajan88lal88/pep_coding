import java.io.*;
import java.util.*;

public class tsp2 {

  public static void targetSumPair(int[] arr, int target) {
    //write your code here
    int j=0;
    while(arr[j]<arr[j+1]&&j<arr.length)
        j++;
    int i=0;
    if(j==arr.length)
    {
        i=0;
    }
    else
        i=j+1;
        
    while(true){
        if(arr[i]+arr[j]==target){
            System.out.println(arr[i]+", "+arr[j]);
            i=(i+1)%arr.length;
            j=(j-1+arr.length)%arr.length;
        }
        else if(arr[i]+arr[j]>target)
        {
            j=(j-1+arr.length)%arr.length;
        }
        else
        {
            i=(i+1)%arr.length;
        }
        if((j+1)%arr.length==i||i==j)
            break;
        
    }
    

  }

  public static void main(String[] args) throws Exception {
    Scanner scn = new Scanner(System.in);
    int n = scn.nextInt();
    int[] arr = new int[n];
    for (int i = 0; i < n; i++) {
      arr[i] = scn.nextInt();
    }
    int target = scn.nextInt();
    targetSumPair(arr,target);
  }

}