import java.io.*;
import java.util.*;

public class sort01 {

  public static void sort(int[] arr){
    //write your code here
    int i=0,j=0;
    for(int t=0;t<arr.length;t++){
        if(arr[t]==1)
            i++;
        else{
            if(arr[t]==0){
                swap(arr,t,j);
                j++;
            }
        }
    }
    
  }

  // used for swapping ith and jth elements of array
  public static void swap(int[] arr, int i, int j) {
    System.out.println("Swapping index " + i + " and index " + j);
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }

  public static void print(int[] arr){
    for(int i = 0 ; i < arr.length; i++){
      System.out.println(arr[i]);
    }
  }
  public static void main(String[] args) throws Exception {
    Scanner scn = new Scanner(System.in);
    int n = scn.nextInt();
    int[] arr = new int[n];
    for(int i = 0 ;i < n; i++){
      arr[i] = scn.nextInt();
    }
    sort(arr);
    print(arr);
  }

}