import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s=new Scanner(System.in);
        int n=s.nextInt();
        int arr[]=new int[n];
        for(int i=0;i<n;i++)
        {
            arr[i]=s.nextInt();
        }
        int k=s.nextInt();
        System.out.println(lastIndex(arr,0,k));
    }

    public static int lastIndex(int[] arr, int idx, int x){
        if(idx==arr.length)
            return -1;
        int pi= lastIndex(arr,idx+1,x);
        if(pi==-1)
        {
            if(arr[idx]==x)
                return idx;
            else
                return -1;
        }
        return pi;
    }
}