import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws Exception {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int m = s.nextInt();
        ArrayList result=getMazePaths(1,1, n, m);
        System.out.println(result);

    }

    public static ArrayList<String> getMazePaths(int sr, int sc, int dr, int dc) {
        if (sr == dr && sc == dc)
        {
            ArrayList<String> temp=new ArrayList<>();
            temp.add("");
            return temp;
        }
        ArrayList <String> rsf=null;
        ArrayList <String> result=new ArrayList<>();
        if(sc+1<=dc)
        {
            rsf=getMazePaths(sr,sc+1,dr,dc);
            for(String val:rsf)
                result.add("h"+val);
        }
        if(sr+1<=dr)
        {
            rsf=getMazePaths(sr+1,sc,dr,dc);
            for(String val:rsf)
                result.add("v"+val);
        }
        return result;
    }

}