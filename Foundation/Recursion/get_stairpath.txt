import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws Exception {
        Scanner s=new Scanner(System.in);
        int n=s.nextInt();
        ArrayList<String> result=getStairPaths(n);
        System.out.println(result);
    }

    public static ArrayList<String> getStairPaths(int n) {
        if(n==0)
        {
            ArrayList<String> temp=new ArrayList<>();
            temp.add("");
            // System.out.println(psf);
            return temp;
        }
        ArrayList<String> result=new ArrayList<>();
        if(n-1>=0)
        {
            ArrayList<String> rsf=getStairPaths(n-1);
            for(String val:rsf)
            {
                result.add(1+val);
            }
        }
        if(n-2>=0)
        {
            ArrayList<String>rsf=getStairPaths(n-2);
            for(String val:rsf)
            {
                result.add(2+val);
            }
        }
        if(n-3>=0)
        {
            ArrayList<String>rsf=getStairPaths(n-3);
            for(String val:rsf)
            {
                result.add(3+val);
            }
        }
        // System.out.println(result);
        return result;
    }

}

