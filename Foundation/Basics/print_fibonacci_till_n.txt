import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
        int n;
        Scanner t = new Scanner(System.in);
        n = t.nextInt();
        int a = 0, b = 1, c;
        for (int i = 0; i < n; i++) {
            System.out.println(a);
            c = a;
            a = b;
            b = a + c;
        }
    }
}