import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int arr1[] = new int[n];
        for (int i = 0; i < n; i++)
            arr1[i] = s.nextInt();
        int m = s.nextInt();
        int arr2[] = new int[m];
        for (int i = 0; i < m; i++)
            arr2[i] = s.nextInt();
        
        int max=n>m?n:m;
        
        int result[]=new int[max];
        max--;
        int i=n-1,j=m-1;
        for(;i>=0&&j>=0;i--,j--)
        {
            
            result[max]+=arr2[j]-arr1[i];
            if(result[max]<0)
            {
                result[max-1]--;
                result[max]+=10;
            }
            
            max--;
        }
        
        while(i>=0)
        {
            result[max--]+=arr1[i--];
        }
        while(j>=0)
        {
            result[max--]+=arr2[j--];
        }
        if(result[0]==0)
            i=1;
        else
            i=0;
        for(;i<(n>m?n:m);i++)
            System.out.println(result[i]);
        

}
}