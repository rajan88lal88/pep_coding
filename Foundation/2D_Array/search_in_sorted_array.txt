import java.io.*;
import java.util.*;

public class Main {
    // public static int binsearch(arr[], int l, int h, int k) {
    //     if (l < h) {
    //         int mid = (l + h) / 2;
    //         if (arr[mid] == k)
    //             return mid;
    //         else if (arr[mid] < k)
    //             return binsearch(arr[], l, mid - 1, k);
    //         return binsearch(arr[], mid + 1, h, k);
    //     }
    //     return -1;
    // }

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int row = s.nextInt();
        int col = row;
        int arr[][] = new int[row][col];
        for (int i = 0; i < row; i++)
            for (int j = 0; j < col; j++)
                arr[i][j] = s.nextInt();

        int k = s.nextInt();
        int i = 0, j = row - 1;
        while (i<row&&j>=0) 
        {
            if (arr[i][j] == k) 
            {
                System.out.println(i + "\n" + j);
                return;
            } 
            else if (k < arr[i][j])
                j--;
            else
                i++;
        }
        System.out.println("Not Found");

    }

}