import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int row = s.nextInt();
        int col = row;
        int arr[][] = new int[row][col];
        for (int i = 0; i < row; i++)
            for (int j = 0; j < col; j++)
                arr[i][j] = s.nextInt();
        int small=0,i=0,j=0,max=0,idx=0,idy=0,t=0;
        while(t<row)
        {
            small=arr[idy][0];
            for(j=0;j<col;j++)
            {
                if(arr[idy][j]<small)
                {
                    small=arr[idy][j];
                    idx=j;
                }
            }
            max=small;
            for(i=0;i<row;i++)
            {
                if(arr[i][idx]>max)
                {
                    max=arr[i][idx];
                    idy=i;
                }
            }
            if(max==small)
            {
                System.out.println(arr[idy][idx]);
                return;
            }
            t++;
        }
        System.out.println("Invalid input");
    }

}