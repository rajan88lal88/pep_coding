import java.util.ArrayList;
import java.util.*;

public class create_tree_stack_1 {
    public static class Node {
        int data;
        ArrayList<Node> child = new ArrayList<>();

        Node(int val) {
            this.data = val;
        }
    }

    public static void main(String[] args) {
        int input[] = { 10, 20, 50, -1, 60, -1, -1, 30, 70, -1, -1, 40, 100, -1, -1, -1 };
        Node root = createTree(input);
        printTree(root);
    }

    private static void printTree(Node root) {
        System.out.print(root.data + "->");
        for (int i = 0; i < root.child.size(); i++) {
            Node node = root.child.get(i);
            System.out.print(node.data + " ");
        }
        System.out.println(".");
        for (int i = 0; i < root.child.size(); i++) {
            Node node = root.child.get(i);
            printTree(node);
        }
    }

    private static Node createTree(int[] arr) {
        Stack<Node> st = new Stack<>();
        Node root = new Node(arr[0]);
        st.push(root);
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == -1)
                st.pop();
            else {
                Node node = new Node(arr[i]);
                Node parent = st.peek();
                parent.child.add(node);
                st.push(node);
            }
        }
        return root;
    }
}